#include "msp430f6638.h"
#include "lcd_serial.h"
void Port_Mapping(void);
void GPIO_Init(void);
void SPI_Init(void);
void ADS_Config(unsigned int config);
int ADS_Read(unsigned char config);
void disp(unsigned char x,unsigned char y,float ADC_Result);
signed int WriteSPI(unsigned int Config, unsigned char mode);
void resultDisplay(unsigned char x,unsigned char y,float digValue, float fsValue);
char csc[20];
char csc1[10];
unsigned int i1;

unsigned int cur_config;
unsigned int vol_config;
unsigned long int timeoutCounter;

 void main(void)
{
  float CUR_Result;
  float VOL_Result;
  cur_config = 0x428b;		//AIN0,4.096V,128SPS
  vol_config = 0x548b;		//AIN1,1.048V,128SPS
  WDTCTL = WDTPW+WDTHOLD;                 // Stop watchdog timer
  Port_Mapping();
  Lcd_Init();
  GPIO_Init();
  SPI_Init();
  DispString57At(0,0,"current:",DRAW_NORMAL);
  DispString57At(2,0,"voltage:",DRAW_NORMAL);


  while(1){
	  ADS_Config(cur_config);
	  GPIO_Init();
	  SPI_Init();
	  for (timeoutCounter = 0; timeoutCounter < 0x000F; timeoutCounter++)
	  	          {
					  CUR_Result = 1.6317*ADS_Read(cur_config);			// Read data from ADS1118
					  disp(1,50,CUR_Result);
	  	              __delay_cycles(1000);
	  	          }
	  for (timeoutCounter = 0; timeoutCounter < 0x00FF; timeoutCounter++)
	  	          {
	  	              __delay_cycles(1000);
	  	          }
	  ADS_Config(vol_config);
	  GPIO_Init();
	  SPI_Init();
	  for (timeoutCounter = 0; timeoutCounter < 0x000F; timeoutCounter++)
	  	          {
					  VOL_Result = 1.6317*ADS_Read(vol_config);			// Read data from ADS1118
					  disp(3,50,VOL_Result);
	  	              __delay_cycles(1000);
	  	          }
	  for (timeoutCounter = 0; timeoutCounter < 0x00FF; timeoutCounter++)
	          {
	              __delay_cycles(1000);
	          }
 }

}

 void GPIO_Init(void)
 {
         P2SEL |= BIT6+BIT4+BIT7;                //
         P2DIR |= BIT4+BIT6+BIT5+BIT7;
 }
void SPI_Init(void)
{
        UCA0CTL1 |= UCSWRST;                      // **Put state machine in reset**
        UCA0CTL0 |= UCMST+UCSYNC+UCCKPL+UCMSB;    // 3-pin, 8-bit SPI master                                          // Clock polarity high, MSB
        UCA0CTL1 |= UCSSEL_2;                     // SMCLK
        UCA0BR0 = 106;                           // /2
        UCA0BR1 = 0;                              //
        UCA0MCTL = 0;                             // No modulation
        UCA0CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
	__delay_cycles(100);                    // 等待初始化完毕
}
void ADS_Config(unsigned int config)
{
	P2OUT &=~ 0x20;						// 置低
	WriteSPI(config,0);				// 写数据
	P2OUT |= 0x20;						// 置高
}
int ADS_Read(unsigned char config)
{
	unsigned int Data;
	P2OUT &=~ 0x20;						// CS置地
	Data = WriteSPI(config,1);		        // 读数据 ADS1118
	P2OUT |= 0x20;						// CS 置高
	return Data;
}

 // Mode 0: 只写
// Mode 1: 读写

signed int WriteSPI(unsigned int config, unsigned char mode)
{
	signed int msb;
	unsigned int temp;
        volatile int nsb;
	temp = config;
	if (mode==1)
          temp = config | 0x8000;
        //写命令
	while(!(UCA0IFG&UCTXIFG));
	UCA0TXBUF = (temp >> 8 );				// 写高位命令
	while(!(UCA0IFG&UCRXIFG));
	nsb = UCA0RXBUF;				        // 读高位命令
	while(!(UCA0IFG&UCTXIFG));
	UCA0TXBUF = (temp & 0xff);				// 写低位命令
	while(!(UCA0IFG&UCRXIFG));
	nsb = (nsb <<8) | UCA0RXBUF;            		// 读低位命令
        //写数据
	while(!(UCA0IFG&UCTXIFG));
	UCA0TXBUF  = (temp >> 8 );				// 写高位数据
	while(!(UCA0IFG&UCRXIFG));
	msb = UCA0RXBUF;					// 读高位数据
	while(!(UCA0IFG&UCTXIFG));
	UCA0TXBUF  = (temp & 0xff);				// 写低位数据
	while(!(UCA0IFG&UCRXIFG));
	msb = (msb << 8) | UCA0RXBUF;			        // 读低位数据
	__delay_cycles(100);
	return msb;
}
void Port_Mapping(void)
{
  // Disable Interrupts before altering Port Mapping registers
  __disable_interrupt();
  // Enable Write-access to modify port mapping registers
  PMAPPWD = 0x02D52;

  #ifdef PORT_MAP_RECFG
  // Allow reconfiguration during runtime
  PMAPCTL = PMAPRECFG;
  #endif

  P2MAP7 = PM_UCA0CLK;
  P2MAP6 = PM_UCA0SOMI;
  P2MAP4 = PM_UCA0SIMO;

  // Disable Write-Access to modify port mapping registers
  PMAPPWD = 0;
  #ifdef PORT_MAP_EINT
  __enable_interrupt();                     // Re-enable all interrupts
  #endif
}
void inttostring(unsigned char n)//转换用的函数
{
  int i=0,j;
  char temp;
  while(n)//先将整数逆序保存进数组
  {
    csc1[i]=n%10+'0';//将十进制数字转换成字符保存
    n=n/10;
    i++;
  }
  if(i==0&&n==0)
  {
    csc1[0]='0';
    i++;
  }
  for(j=i-1;j>=i/2;j--)//再将逆序的字符结果再逆转一次，得到正确结果
  {
    temp=csc1[j];
    csc1[j]=csc1[i-j-1];
    csc1[i-j-1]=temp;
  }
  csc1[i]='.';
  i1=i;
}
void disp(unsigned char x,unsigned char y,float ADC_Result)
{
  //test display
  int i,adc_r2;
  unsigned long adc_r1;
  float adc_r;
//  lcd_clear();
  adc_r=(ADC_Result/65535)*5;
  adc_r2=adc_r;
  adc_r=adc_r-adc_r2;
  inttostring(adc_r2);
  for(i=0;i<5;i++)
  {
     adc_r1=adc_r*10;
     adc_r1=adc_r1%10;
     csc[i]=adc_r1+'0';
     adc_r=adc_r*10;
  }
  csc[i+1]='v';
  DispString57At(x,y,csc1,DRAW_NORMAL);//打印字符串
  DispString57At(x,y + (i1+1)*6,csc,DRAW_NORMAL);//打印字符串
  __delay_cycles(2500);
}

///**
// *
// * 数字量结果显示函数
// */
//void resultDisplay(unsigned char x,unsigned char y,float digValue, float fsValue){
//	DispFloat57At(x,y,digValue/65536*fsValue,2,6,DRAW_NORMAL);
//}


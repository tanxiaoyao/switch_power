/*
 * sys.h
 *
 *  Created on: 2013-8-15
 *      Author: Administrator
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_


#define PMM_STATUS_OK     0
#define PMM_STATUS_ERROR  1
#define _HAL_PMM_SVMLE  (SVMLE)
#define _HAL_PMM_SVSLE  (SVSLE)
#define _HAL_PMM_SVSFP  (SVSLFP)
#define _HAL_PMM_SVMFP  (SVMLFP)

void EnableX1Clk(void);
void EnableX2Clk(void);
unsigned int SetVCore (unsigned char level);
static unsigned int SetVCoreUp (unsigned char level);
void SetInternalDCO(void);
void SysCtrlInit(void);
void SelSysClk(void);

/*******************************************************************************/
/******************************* 系统初始化程序**************************************/
/*******************************************************************************/
void SysCtrlInit(void)
{
	P5DIR|=BIT4;
	P5OUT&=~BIT4;

	//SetVCore(PMMCOREV_0);                     // Set VCore = 1.4V for 4-8MHz clock
	//SetVCore(PMMCOREV_1);                     // Set VCore = 1.6V for 8-16MHz clock
	//SetVCore(PMMCOREV_2);                     // Set VCore = 1.9V for 16-24MHz clock
	SetVCore(PMMCOREV_3);                    	// Set VCore = 1.8V for 24-32MHz clock

	// 使能外部晶振XT1
	EnableX1Clk();
	// 使能外部晶振XT2
	// EnableX2Clk();

	UCSCTL4 |= SELA_0;                        	// ACLK = LFTX1 (by default)

	// 配置内部DCO振荡器
	SetInternalDCO();

	// 6638ACLK、SMCLK、MCLK时钟源选择
	UCSCTL4 |= SELS__DCOCLKDIV + SELM__DCOCLKDIV;  	// SMCLK=MCLK=DCOCLKDIV,如果外部晶振可以起振。。。。。。
}

void SetInternalDCO(void)
{
	__bis_SR_register(SCG0);                  // Disable the FLL control loop
	UCSCTL0 = 0x0000;                         // Set lowest possible DCOx=0, MODx=0
//  UCSCTL1 = DCORSEL_0;                      // Set RSELx for DCO = 0.07- 0.2 MHz
//  UCSCTL1 = DCORSEL_1;                      // Set RSELx for DCO = 0.15-0.36 MHz
//  UCSCTL1 = DCORSEL_2;                      // Set RSELx for DCO = 0.32-0.75 MHz
//  UCSCTL1 = DCORSEL_3;                      // Set RSELx for DCO = 0.64-1.51 MHz
//  UCSCTL1 = DCORSEL_4;                      // Set RSELx for DCO = 1.3-3.2   MHz
//  UCSCTL1 = DCORSEL_5;                      // Set RSELx for DCO = 2.5-6.0   MHz
//  UCSCTL1 = DCORSEL_6;                      // Set RSELx for DCO = 4.6-10.7  MHz
	UCSCTL1 = DCORSEL_7;                      // Set RSELx for DCO = 8.5-19.6  MHz
//  UCSCTL0 = 0x0f00;                         // Set lowest possible DCOx=31, MODx=0
//  UCSCTL1 = DCORSEL_0;                      // Set RSELx for DCO = 0.70- 1.7 MHz
//  UCSCTL1 = DCORSEL_1;                      // Set RSELx for DCO = 1.47-3.45 MHz
//  UCSCTL1 = DCORSEL_2;                      // Set RSELx for DCO = 3.17-7.38 MHz
//  UCSCTL1 = DCORSEL_3;                      // Set RSELx for DCO = 6.07-14.0 MHz
//  UCSCTL1 = DCORSEL_4;                      // Set RSELx for DCO = 12.3-28.2 MHz
//  UCSCTL1 = DCORSEL_5;                      // Set RSELx for DCO = 23.7-54.1 MHz
//  UCSCTL1 = DCORSEL_6;                      // Set RSELx for DCO = 39.0-88.0 MHz
//  UCSCTL1 = DCORSEL_7;                      // Set RSELx for DCO = 60.0-135  MHz
//  UCSCTL2 = FLLD__1 + 3;
//  UCSCTL2 = FLLD_1 + 30;                    // (30 + 1) * 32768 = 1MHz
	                                              // (N + 1) * FLLRef = Fdco
	                                              // Set FLL Div = fDCOCLK/2
//  UCSCTL2 = FLLD_1 + 60;                    // (60 + 1) * 32768 = 2MHz
//  UCSCTL2 = FLLD_1 + 121;                   // (121 + 1) * 32768 = 4MHz
//  UCSCTL2 = FLLD_1 + 243;                   // (243 + 1) * 32768 = 8MHz
//  UCSCTL2 = FLLD_1 + 365;                   // (365 + 1) * 32768 = 12MHz
//  UCSCTL2 = FLLD_1 + 487;                   // (487 + 1) * 32768 = 16MHz

//	以下为超频，不安全，不稳定
//  UCSCTL2 = FLLD_1 + 610;                   // (610 + 1) * 32768 = 20MHz
//  UCSCTL2 = FLLD_1 + 732;                   // (732 + 1) * 32768 = 24MHz
//  UCSCTL2 = FLLD_1 + 763;                   // (763 + 1) * 32768 = 25MHz
//  UCSCTL2 = FLLD_1 + 916;                   // (916 + 1) * 32768 = 30MHz
	UCSCTL2 = FLLD_1 + 977;                   // (977 + 1) * 32768 = 32MHz
//  UCSCTL2 = FLLD_1 + 1023;                  // (1023 + 1) * 32768 = 33.55MHz
//	以上为超频，不安全，不稳定

//  UCSCTL2 = FLLD_1 + 60;                    // (60 + 1) * 32768 * 0.5 = 1MHz
	                                              // (N + 1) * FLLRef = Fdco
	                                              // Set FLL Div = fDCOCLK/2
//  UCSCTL2 = FLLD_1 + 121;                   // (121 + 1) * 32768 * 0.5 = 2MHz
//  UCSCTL2 = FLLD_1 + 243;                   // (243 + 1) * 32768 * 0.5 = 4MHz
//  UCSCTL2 = FLLD_1 + 487;                   // (487 + 1) * 32768 * 0.5 = 8MHz
//  UCSCTL2 = FLLD_1 + 731;                   // (731 + 1) * 32768 * 0.5 = 12MHz
//  UCSCTL2 = FLLD_1 + 976;                   // (976 + 1) * 32768 * 0.5 = 16MHz
//  UCSCTL2 = FLLD_1 + 1098;                  // (1098 + 1) * 32768 * 0.5 = 18MHz

	__bic_SR_register(SCG0);                    // Enable the FLL control loop
}

/*******************************************************************************/
/*******************************外部晶振起振程序**************************************/
/*******************************************************************************/
void EnableX1Clk(void)
{
	while(BAKCTL & LOCKBAK)                    	// Unlock XT1 pins for operation
		BAKCTL &= ~(LOCKBAK);
	UCSCTL6 &= ~(XT1OFF);                     	// XT1 On
	UCSCTL6 |= XCAP_3;                        	// Internal load cap

	// Loop until XT1 fault flag is cleared
	do
	{
		UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + DCOFFG);
	                                             	// Clear XT2,XT1,DCO fault flags
	    SFRIFG1 &= ~OFIFG;                     	 	// Clear fault flags
	}while (SFRIFG1&OFIFG);                   	 	// Test oscillator fault flag

	UCSCTL6 &= ~(XT1DRIVE_3);                 	 	// Xtal is now stable, reduce drive strength
}
void EnableX2Clk(void)
{
	P7SEL |= BIT2+BIT3;                       	// Port select XT2

	UCSCTL6 &= ~XT2OFF;                       	// Enable XT2
	UCSCTL3 |= SELREF__XT2CLK;                	// FLLref = XT2

	// UCSCTL4 |= SELA__REFOCLK;              	// ACLK=REFO,SMCLK=DCO,MCLK=DCO

	// Loop until XT1,XT2 & DCO stabilizes - in this case loop until XT2 settles
	do
	{
		UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + XT1HFOFFG + DCOFFG);
	                                            	// Clear XT2,XT1,DCO fault flags
	    SFRIFG1 &= ~OFIFG;                      	// Clear fault flags
	}while (SFRIFG1&OFIFG);                   	// Test oscillator fault flag

	UCSCTL6 &= ~XT2DRIVE0;                    	// Decrease XT2 Drive according to

	// UCSCTL4 |= SELS__XT2CLK + SELM__XT2CLK;   	// SMCLK=MCLK=XT2
}

/*******************************************************************************/
/*******************************核心电压设置程序**************************************/
/*******************************************************************************/
unsigned int SetVCore (unsigned char level)
{
	unsigned int actlevel;
	unsigned int status = 0;
	level &= PMMCOREV_3;                       // Set Mask for Max. level
	actlevel = (PMMCTL0 & PMMCOREV_3);         // Get actual VCore

	while (((level != actlevel) && (status == 0)) || (level < actlevel))		// step by step increase or decrease
	{
		if (level > actlevel)
			status = SetVCoreUp(++actlevel);
	}
	return status;
}

static unsigned int SetVCoreUp (unsigned char level)
{
	unsigned int PMMRIE_backup,SVSMHCTL_backup;

	// Open PMM registers for write access
	PMMCTL0_H = 0xA5;

	// Disable dedicated Interrupts to prevent that needed flags will be cleared
	PMMRIE_backup = PMMRIE;
	PMMRIE &= ~(SVSMHDLYIE | SVSMLDLYIE | SVMLVLRIE | SVMHVLRIE | SVMHVLRPE);
	SVSMHCTL_backup = SVSMHCTL;
	PMMIFG &= ~(SVMHIFG | SVSMHDLYIFG);
	// Set SVM highside to new level and check if a VCore increase is possible
	SVSMHCTL = SVMHE | SVSHE | (SVSMHRRL0 * level);
	// Wait until SVM highside is settled
	while ((PMMIFG & SVSMHDLYIFG) == 0);
	// Check if a VCore increase is possible
	if ((PMMIFG & SVMHIFG) == SVMHIFG)			//-> Vcc is to low for a Vcore increase
	{
		// recover the previous settings
		PMMIFG &= ~SVSMHDLYIFG;
		SVSMHCTL = SVSMHCTL_backup;
		// Wait until SVM highside is settled
		while ((PMMIFG & SVSMHDLYIFG) == 0);
		// Clear all Flags
		PMMIFG &= ~(SVMHVLRIFG | SVMHIFG | SVSMHDLYIFG | SVMLVLRIFG | SVMLIFG | SVSMLDLYIFG);
		// backup PMM-Interrupt-Register
		PMMRIE = PMMRIE_backup;

		// Lock PMM registers for write access
		PMMCTL0_H = 0x00;
		return PMM_STATUS_ERROR;            // return: voltage not set
	}
	// Set also SVS highside to new level	    //-> Vcc is high enough for a Vcore increase
	SVSMHCTL |= (SVSHRVL0 * level);
	// Set SVM low side to new level
	SVSMLCTL = SVMLE | (SVSMLRRL0 * level);
	// Wait until SVM low side is settled
	while ((PMMIFG & SVSMLDLYIFG) == 0);
	// Clear already set flags
	PMMIFG &= ~(SVMLVLRIFG | SVMLIFG);
	// Set VCore to new level
	PMMCTL0_L = PMMCOREV0 * level;
	// Wait until new level reached
	if (PMMIFG & SVMLIFG)
		while ((PMMIFG & SVMLVLRIFG) == 0);
	// Set also SVS/SVM low side to new level
	PMMIFG &= ~SVSMLDLYIFG;
	SVSMLCTL |= SVSLE | (SVSLRVL0 * level);
	// wait for lowside delay flags
	while ((PMMIFG & SVSMLDLYIFG) == 0);

	// Disable SVS/SVM Low
	// Disable full-performance mode to save energy
	SVSMLCTL &= ~(_HAL_PMM_SVSLE + _HAL_PMM_SVMLE + _HAL_PMM_SVSFP + _HAL_PMM_SVMFP);
	SVSMHCTL &= ~(_HAL_PMM_SVSFP + _HAL_PMM_SVMFP);

	// Clear all Flags
	PMMIFG &= ~(SVMHVLRIFG | SVMHIFG | SVSMHDLYIFG | SVMLVLRIFG | SVMLIFG | SVSMLDLYIFG);
	// backup PMM-Interrupt-Register
	PMMRIE = PMMRIE_backup;

	// Lock PMM registers for write access
	PMMCTL0_H = 0x00;
	return PMM_STATUS_OK;                               // return: OK
}


#endif /* SYSTEM_H_ */

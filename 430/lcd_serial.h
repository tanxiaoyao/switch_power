/*
 * lcd_serial.h
 *
 *  Created on: 2013-8-16
 *      Author: Administrator
 */

#ifndef LCD_SERIAL_H_
#define LCD_SERIAL_H_

// Macros
#ifndef abs
#define abs(n)     (((n) < 0) ? -(n) : (n))
#endif

#include "ASCII.h"

#define SET_CS  (P8OUT|=BIT7)
#define CLR_CS  (P8OUT&=~BIT7)
#define SET_A0  (P8OUT|=BIT6)
#define CLR_A0  (P8OUT&=~BIT6)

#define DRAW_NORMAL   0x00   // Display dark pixels on a light background
#define DRAW_INVERT   0x01   // Display light pixels on a dark background

const long numtab[]={
  1,10,100,1000,10000,100000,1000000,10000000,100000000,1000000000,10000000000};

/**************************************************************
 * 						函数申明
 *
 **************************************************************/
unsigned char SPIB1_Txdat(unsigned char dat);
void SPIB1_Init(void);
void wr_dat(unsigned char dat);
void wr_cmd(unsigned char cmd);
void lcd_init();
void lcd_clear(void);
unsigned char column_trans_h(unsigned char column_value);	//列数转换为lcd规定的高四位和低四位指令
unsigned char column_trans_l(unsigned char column_value);
void DispStringAt(unsigned char x,unsigned char y,char *str);

void display_graphic_8x16(unsigned int page,unsigned int column,const unsigned char *dp);

/**************************************************************
 * 						SPI_transmit
 *
 **************************************************************/
unsigned char SPIB1_Txdat(unsigned char dat)
{
	while (!(UCB1IFG&UCTXIFG));       // USCI_A0 TX buffer ready?
	UCB1TXBUF = dat;                    // Transmit first character
	while(!(UCB1IFG&UCRXIFG));
//	UCB1IFG&=~UCRXIFG;
	return UCB1RXBUF;
}
void SPIB1_Init(void)
{
	P8SEL |= BIT4+BIT5;                  	// Assign P2.0 to UCB0CLK and...
	P8DIR |= BIT4+BIT5;                 	// P2.1 UCB0SOMI P2.2 UCB0SIMO

	UCB1CTL1 |= UCSWRST;                      // **Put state machine in reset**
	UCB1CTL0 |= UCMST+UCSYNC+UCCKPL+UCMSB;    // 3-pin, 8-bit SPI master
	                                          // Clock polarity high, MSB
	UCB1CTL1 |= UCSSEL_2;                     // SMCLK
	UCB1BR0 = 0x04;                           // SMCLK/4 = 8Mhz
	UCB1BR1 = 0;                              //
//  UCB1MCTL = 0;                             // No modulation
	UCB1CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
//  UCA0IE = UCRXIE ;                         // Enable USCI_A0 RX,TX interrupt
}

/**************************************************************
 * 						底层函数
 *
 **************************************************************/
/*=======写指令========*/
void wr_cmd(unsigned char cmd)
{
	CLR_CS;
	CLR_A0;
	SPIB1_Txdat(cmd);
	SET_CS;
}

/*--------写数据------------*/
void wr_dat(unsigned char dat)
{
	CLR_CS;
	SET_A0;
	SPIB1_Txdat(dat);
	SET_CS;
}
/*--------读数据------------*//////这个函数目前是错的
unsigned char rd_dat(void) //read a 8bit character
{
	SPIB1_Txdat(0xFF);//clear buffer
	return UCB1RXBUF;
}
//TODO 这里完成读函数以后后面的几何绘图函数就可以使用
//
//unsigned char rd_dat(unsigned char address)
//{
//	unsigned char rd_dat;
//	CLR_CS;
//	SET_A0;
//	rd_dat = SPIB1_Txdat(address);
//	SET_CS;
//	return rd_dat;
//}
///*--------从坐标读数据------------*/
//unsigned char ReadDataFrom(unsigned char page,unsigned char column){
//	lcd_addr(page,column);
//	rd_dat();
//}

/**************************************************************
 * 						初始化
 *
 **************************************************************/
void lcd_clear(void)
{
	unsigned char i,j;
	for(i=0;i<9;i++)
	{
		wr_cmd(0xb0+i);
		wr_cmd(0x10);
		wr_cmd(0x00);
		for(j=0;j<132;j++)
		{
			wr_dat(0x00);
		}
	}
}

void Lcd_Init()
{
	SPIB1_Init();
	P8DIR|=BIT6+BIT7;
	SET_CS;
	wr_cmd(0xe2);    /*软复位*/
	wr_cmd(0x2c);    /*升压步聚 1*/
	wr_cmd(0x2e);    /*升压步聚 2*/
	wr_cmd(0x2f);    /*升压步聚 3*/
	wr_cmd(0x23);    /*粗调对比度，可设置范围 20～27*/
	wr_cmd(0x81);    /*微调对比度*/
	wr_cmd(0x28);    /*微调对比度的值，可设置范围 0～63*/
	wr_cmd(0xa2);    /*1/9 偏压比（bias）*/
	wr_cmd(0xc8);    /*行扫描顺序：从上到下*/
	wr_cmd(0xa0);    /*列扫描顺序：从左到右*/
	wr_cmd(0xaf);    /*开显示*/
	lcd_clear();
}
void delay(double time){
	while(time--);
}
/**************************************************************
 * 						显示
 *
 **************************************************************/

void lcd_addr(unsigned char page,unsigned char column)
{
	column=column;
	wr_cmd(0xb0+page); /*设置页地址*/
	wr_cmd(0x10+(column>>4&0x0f)); /*设置列地址的高4 位*/
	wr_cmd(column&0x0f); /*设置列地址的低4 位*/
}

/*显示8x16 点阵图像、ASCII, 或8x16 点阵的自造字符、其他图标*/
void disp_graph_8x16(unsigned char page,unsigned char column,const unsigned char *dp)
{
	unsigned char i,j;
	for(j=0;j<2;j++)
	{
		lcd_addr(page+j,column);
		for (i=0;i<8;i++)
		{
			wr_dat(*dp); /*写数据到LCD,每写完一个8 位的数据后列地址自动加1*/
			dp++;
		}
	}
}
/*显示 16x16 点阵图像、汉字、生僻字或 16x16 点阵的其他图标*/
void disp_graph_16x16(unsigned char page,unsigned char column,const unsigned char *dp)
{
	unsigned char i,j;
	for(j=0;j<2;j++)
	{
		lcd_addr(page+j,column);
		for (i=0;i<16;i++)
		{
			wr_dat(*dp);     /*写数据到 LCD,每写完一个 8 位的数据后列地址自动加 1*/
			dp++;
		}
	}
}
/*显示 32x32 点阵图像、汉字、生僻字或 32x32 点阵的其他图标*/
void disp_graph_32x32(unsigned char page,unsigned char column,const unsigned char *dp)
{
	unsigned char i,j;
	for(j=0;j<4;j++)
	{
		lcd_addr(page+j,column);
		for (i=0;i<32;i++)
		{
			wr_dat(*dp);     /*写数据到 LCD,每写完一个 8 位的数据后列地址自动加 1*/
			dp++;
		}
	}
}
/*显示 5*7 点阵图像、ASCII, 或 5x7 点阵的自造字符、其他图标*/
void disp_graph_5x7(unsigned char page,unsigned char column,const unsigned char *dp,unsigned char style)
{
	unsigned char i;
 	lcd_addr(page,column);
 	if (style == DRAW_NORMAL)
 	    {
 	        // write character
			for (i=0;i<6;i++)
			{
				wr_dat(*dp);
				dp++;
			}
 	    }
 	    else
 	    {
 	    	for (i=0;i<6;i++)
			{
				wr_dat(*dp ^ 0xFF);
				dp++;
			}
 	    }
}
/**************************************************************
 * 						高层显示
 *
 **************************************************************/
/*=======16*16========*/
void DispWelcomAt(unsigned char x,unsigned char y,char *str){			//显示16*16的自造字库字符，str必须为字库集内码
	unsigned char i;
	  for(i=0;;i++)
	  {
	    if(str[i]==0)
	      break;
	    disp_graph_16x16(x,y+i*16,DDS_welcome[str[i] - '0']);
	  }
}
/*显示9x16 点阵图像、ASCII, 或8x16 点阵的自造字符、其他图标*/
void disp_graph_9x16(unsigned char page,unsigned char column,const unsigned char *dp,unsigned int style)
{
	unsigned char i,j;
	for(j=0;j<2;j++)
	{
		lcd_addr(page+j,column);
		if (style == DRAW_NORMAL)
		 	    {
		 	        // write character
					for (i=0;i<9;i++)
					{
						wr_dat(*dp);
						dp++;
					}
		 	    }
		 	    else
		 	    {
		 	    	for (i=0;i<9;i++)
					{
						wr_dat(*dp ^ 0xFF);
						dp++;
					}
		 	    }
	}
}
/*=======9*16加粗字符串========*/
void DispString916At(unsigned char x,unsigned char y,char *str,unsigned int style)
{
  unsigned char i;
  for(i=0;;i++)
  {
    if(str[i]==0)
      break;
    disp_graph_9x16(x,y+i*9,ASCII9_16bold[str[i]-'0'],style);
  }
}
/**
 *
 * 按位反转显示加粗字符串
 */
void DispString916WithOneInvert(unsigned char x,unsigned char y,char *str,unsigned int invertAt)
{
  unsigned char i;
  for(i=0;;i++)
  {
    if(str[i]==0)
      break;
    if(i == invertAt - 1){
    	disp_graph_9x16(x,y+i*9,ASCII9_16bold[str[i]-'0'],DRAW_INVERT);
    }else
    disp_graph_9x16(x,y+i*9,ASCII9_16bold[str[i]-'0'],DRAW_NORMAL);
  }
}
/*=======9*16加粗整型数字========*/
void DispDec916At(unsigned char x,unsigned char y,int dat,unsigned char len,unsigned int style)
{
	char str[12];
	unsigned char i,dl;
	str[len]=0;
	for(i=0;i<len;i++)
	{
		dl=dat%10;
		dat/=10;
		str[len-i-1]=dl+'0';
	}
	DispString916At(x,y,str,style);
}
/*=======9*16按位反转显示加粗整型数字========*/
void DispDec916WithOneInvert(unsigned char x,unsigned char y,int dat,unsigned char len,unsigned int invertAt)
{
	char str[12];
	unsigned char i,dl;
	str[len]=0;
	for(i=0;i<len;i++)
	{
		dl=dat%10;
		dat/=10;
		str[len-i-1]=dl+'0';
	}
	DispString916WithOneInvert(x,y,str,invertAt);
}

/**
 *
 * 显示长度较大的加粗浮点型小数
 */
void DispSmall916At(unsigned char x,unsigned char y,float dat,unsigned char len1,unsigned char len2 ,unsigned int style)
{
	int dat1;
    long dat2;
	dat1=(int)dat;
	dat2=(long)((dat-dat1)*numtab[len2]);
	DispDec916At(x,y,dat1,len1,style);
	DispStringAt(x,y+9*len1,".");
	char str[12];
	unsigned char i,dl;
	str[len2]=0;
	for(i=0;i<len2;i++)
	{
		dl=dat2%10;
		dat2/=10;
		str[len2-i-1]=dl+'0';
	}
	DispString916At(x,y+9*(len1+1),str,style);
}
/**
 *
 * 按位反转显示字符串函数，用于设置某些值的时候引用
 */
void DispSmall916WithOneInvert(unsigned char x,unsigned char y,float dat,unsigned char len1,unsigned char len2 ,unsigned int invertAt){
	int dat1;
    long dat2;
	dat1=(int)dat;
	dat2=(long)((dat-dat1)*numtab[len2]);
	if(invertAt <= len1){
		DispDec916At(x,y,dat1,len1,invertAt);
	}else{
		DispDec916At(x,y,dat1,len1,DRAW_NORMAL);
	}
	DispStringAt(x,y+9*len1,".");
	char str[12];
	unsigned char i,dl;
	str[len2]=0;
	for(i=0;i<len2;i++)
	{
		dl=dat2%10;
		dat2/=10;
		str[len2-i-1]=dl+'0';
	}
	if(invertAt > len1){
		DispString916WithOneInvert(x,y+9*(len1+1),str,invertAt - len1);
	}else{
	DispString916At(x,y+9*(len1+1),str,DRAW_NORMAL);
	}
}


/*=======8*16========*/
void DispStringAt(unsigned char x,unsigned char y,char *str)
{
  unsigned char i;
  for(i=0;;i++)
  {
    if(str[i]==0)
      break;
    disp_graph_8x16(x,y+i*8,ASCII8_16[str[i]-' ']);
  }
}

void DispDecAt(unsigned char x,unsigned char y,int dat,unsigned char len)
{
	char str[12];
	unsigned char i,dl;
	str[len]=0;
	for(i=0;i<len;i++)
	{
		dl=dat%10;
		dat/=10;
		str[len-i-1]=dl+'0';
	}
	DispStringAt(x,y,str);
}
void DispHexAt(unsigned char x,unsigned char y,long dat,unsigned char len)
{
	unsigned char i,dl;
	char str[12];
	for(i=0;i<len;i++)
	{
		dl=dat&0xf;
		dat=dat>>4;
		if(dl<10)
			str[len-i-1]=dl+'0';
		else
			str[len-i-1]=dl-10+'a';
	}
	DispStringAt(x,y,str);
}
void DispFloatAt(unsigned char x,unsigned char y,float dat,unsigned char len1,unsigned char len2 )
{
	int dat1,dat2;
	dat1=(int)dat;
	dat2=(int)((dat-dat1)*numtab[len2]);
	DispDecAt(x,y,dat1,len1);
	DispStringAt(x,y+8*len1,".");
	DispDecAt(x,y+8*(len1+1),dat2,len2);
}
/*=======5*7========*/
void DispString57At(unsigned char x,unsigned char y,char *str,unsigned int style)
{
	  unsigned char i;
	  for(i=0;;i++)
	  {
	    if(str[i]==0)
	      break;
	    disp_graph_5x7(x,y+i*6,ASCII5_7[str[i]-' '],style);
	  }
}
void DispDec57At(unsigned char x,unsigned char y,int dat,unsigned char len,unsigned int style)
{
	char str[12];
	unsigned char i,dl;
	str[len]=0;
	for(i=0;i<len;i++)
	{
		dl=dat%10;
		dat/=10;
		str[len-i-1]=dl+'0';
	}
	DispString57At(x,y,str,style);
}
void DispHex57At(unsigned char x,unsigned char y,long dat,unsigned char len,unsigned int style)
{
	unsigned char i,dl;
	char str[12];
	str[len]=0;
	for(i=0;i<len;i++)
	{
		dl=dat&0xf;
		dat=dat>>4;
		if(dl<10)
			str[len-i-1]=dl+'0';
		else
			str[len-i-1]=dl-10+'a';
	}
	DispString57At(x,y,str,style);
}
void DispFloat57At(unsigned char x,unsigned char y,float dat,unsigned char len1,unsigned char len2 ,unsigned int style)
{
	int dat1,dat2;
	dat1=(int)dat;
	dat2=(int)((dat-dat1)*numtab[len2]);
	DispDec57At(x,y,dat1,len1,style);
	DispString57At(x,y+6*len1,".",style);
	DispDec57At(x,y+6*(len1+1),dat2,len2,style);
}
void DispDouble57At(unsigned char x,unsigned char y,double dat,unsigned char len1,unsigned char len2 ,unsigned int style)
{
	int dat1,dat2;
	dat1=(int)dat;
	dat2=(int)((dat-dat1)*numtab[len2]);
	DispDec57At(x,y,dat1,len1,style);
	DispString57At(x,y+6*len1,".",style);
	DispDec57At(x,y+6*(len1+1),dat2,len2,style);
}
//显示长度较大的浮点型小数
void DispSmall57At(unsigned char x,unsigned char y,float dat,unsigned char len1,unsigned char len2 ,unsigned int style)
{
	int dat1;
    long dat2;
	dat1=(int)dat;
	dat2=(long)((dat-dat1)*numtab[len2]);
	DispDec57At(x,y,dat1,len1,style);
	DispString57At(x,y+6*len1,".",style);
	char str[12];
	unsigned char i,dl;
	str[len2]=0;
	for(i=0;i<len2;i++)
	{
		dl=dat2%10;
		dat2/=10;
		str[len2-i-1]=dl+'0';
	}
	DispString57At(x,y+6*(len1+1),str,style);
}
//显示一张全屏图片
void DispImg12864(const unsigned char *graphic){
	const unsigned char *address;		//声明数据指针地址
	int i,j;
	address = graphic;
	for(i = 0;i < 8;i ++){
		CLR_CS;	//	屏幕片选清零
		wr_cmd(0xb0 + i);	//set page address
		wr_cmd(0x10);
		wr_cmd(0x00);
		for(j = 0;j < 128;j ++){
			wr_dat(*address);
			address ++;
		}
	}
}

/**************************************************************
 * 						动画效果
 *
 **************************************************************/
/*
 * 散点擦除屏幕动画效果
 */
void EraseScreen(){
	unsigned char i,j;
	wr_cmd(0x10);
	wr_cmd(0x00);
//	从左到右填充
	for(j=0;j<128;j++){
		for(i=0;i<8;i++)
			{
				__delay_cycles(2000);
				wr_cmd(column_trans_h(j));
				wr_cmd(column_trans_l(j));
				wr_cmd(0xb0+i);
				wr_dat(0x55);
			}
		j ++;
		for(i=0;i<8;i++)
			{
				__delay_cycles(2000);
				wr_cmd(column_trans_h(j));
				wr_cmd(column_trans_l(j));
				wr_cmd(0xb0+i);
				wr_dat(0xAA);
			}
	}
//	从左到右擦除
	for(j=0;j<128;j++){
			for(i=0;i<8;i++)
				{
					__delay_cycles(2000);
					wr_cmd(column_trans_h(j));
					wr_cmd(column_trans_l(j));
					wr_cmd(0xb0+i);
					wr_dat(0x00);
				}
	}
	lcd_clear();//清屏
}

/*
 * 将128个列值转换为相应的列值高四位和低四位
 */
unsigned char column_trans_h(unsigned char column_value){
	return (column_value>>4)&0x0f|0x10;
}
unsigned char column_trans_l(unsigned char column_value){
	return (column_value)&0x0f;
}

#endif /* LCD_SERIAL_H_ */

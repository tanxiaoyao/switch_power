#include "BUS_FPGA.h"
#include "lcd_serial.h"
//#include "uart.h"
#define TEST_BUS_BASE  CS1

//变量声明
unsigned int keyvalue;
extern unsigned int currentItem;
extern unsigned int wave_kind_num;
extern char *a_title;
extern char *b_title;
extern char *c_title;
extern char *d_title;
extern float freq_disp;		//全局频率显示值
extern float freq_value;	//全局频率实际值

#define KEY_BASE CS0

//函数声明
void normalPage(unsigned int key);
void flash2morePage();
void setTab(char *a_string,char *b_string,char *c_string,char *d_string,unsigned int a_style,unsigned int b_style,unsigned int c_style,unsigned int d_style);
void kindSetPage(unsigned int key);
void morePage(unsigned int key);
extern void flashMainView();
extern void flashSetting(unsigned char CS,unsigned int fre_reg_h,unsigned int fre_reg_l,
		unsigned int pha_reg_h,unsigned int pha_reg_l,
		unsigned int range_reg,unsigned int wave_select);
void backFromMorePage();
void freqSetPage(unsigned int key);
void tabDispInvert(char *a_string,char *b_string,char *c_string,char *d_string,unsigned int invertAt);
void valueDispInvert(float dispValue,unsigned int invertAt);

/**
 * 按键按下响应函数
 * in:当前是那个页面
 * 最后跳转到对应页面的键值处理函数
 */
void keyPressed(){
	keyvalue = IORD(KEY_BASE,0);
	switch(currentItem){
	case 0:
			normalPage(keyvalue);			//当前是常规显示界面
		break;
	case 1:
			freqSetPage(keyvalue);			//当前正处于频率设置界面
			break;
	case 2:
			//pahsSetPage(keyvalue);
			break;
	case 3:
			//rangSetPage(keyvalue);
			break;
	case 4:
			morePage(keyvalue);				//当前是“更多”常规显示界面
			break;
	case 5:
			//kindSetPage(keyvalue);
			break;
	case 6:
			//chanSetPage(keyvalue);
			break;
	}

}

/**
 *
 * 常规显示界面按键处理函数
 */
void normalPage(unsigned int key){
	switch(key){
	case 0x88:					//按下more按键后写入当前页面值，转入“更多”界面
		currentItem = 4;
		flash2morePage();
		break;
	case 0x18:					//按下进入频率设置按键
		currentItem = 1;
		DispStringAt(3,35,"         ");		//清空中间的显示
		tabDispInvert(a_title,b_title,c_title,d_title,1);	//反转底栏频率项
		break;
	}
}

/**
 *
 * 频率设置界面按键响应函数
 */
void freqSetPage(unsigned int key){
	switch(key){
	}
}

/**
 *
 * 更多显示界面按键处理函数
 */
void morePage(unsigned int key){
	switch(key){
	case 0x18:
		if(wave_kind_num < 3){
			wave_kind_num ++;
		}else wave_kind_num = 0;
		flashSetting(TEST_BUS_BASE,0,1024,0,0,0,wave_kind_num);					//传入设置参数
		flashMainView();
		break;
	case 0x88:
		currentItem = 0;
		backFromMorePage();
	}

}

///**
// *
// * 波形设置按键响应函数
// */
//void kindSetPage(unsigned int key){
//
//}
/**
 *
 * 转向“更多”界面函数
 */
void flash2morePage(){
	a_title = "kind";			//切换底栏显示
	b_title = "chan";
	c_title = "    ";
	d_title = "back";
	setTab(a_title,b_title,c_title,d_title,DRAW_NORMAL,DRAW_NORMAL,DRAW_NORMAL,DRAW_NORMAL);
}
/**
 *
 * 退出“更多”界面函数
 */
void backFromMorePage(){
	a_title = "freq";			//切换底栏显示
	b_title = "pahs";
	c_title = "rang";
	d_title = "more";
	setTab(a_title,b_title,c_title,d_title,DRAW_NORMAL,DRAW_NORMAL,DRAW_NORMAL,DRAW_NORMAL);
}

/**
 *
 * 底栏菜单设置函数
 */
void setTab(char *a_string,char *b_string,char *c_string,char *d_string,unsigned int a_style,unsigned int b_style,unsigned int c_style,unsigned int d_style){
	DispString57At(7,4,a_string,a_style);			//显示底栏菜单
	DispString57At(7,37,b_string,b_style);
	DispString57At(7,68,c_string,c_style);
	DispString57At(7,100,d_string,d_style);
}

/**
 *
 *
 * tab显示反转函数
 */
void tabDispInvert(char *a_string,char *b_string,char *c_string,char *d_string,unsigned int invertAt){
	switch(invertAt){
	case 1:
	setTab(a_string,b_string,c_string,d_string,DRAW_INVERT,DRAW_NORMAL,DRAW_NORMAL,DRAW_NORMAL);
	break;
	case 2:
	setTab(a_string,b_string,c_string,d_string,DRAW_NORMAL,DRAW_INVERT,DRAW_NORMAL,DRAW_NORMAL);
	break;
	case 3:
	setTab(a_string,b_string,c_string,d_string,DRAW_NORMAL,DRAW_NORMAL,DRAW_INVERT,DRAW_NORMAL);
	break;
	case 4:
	setTab(a_string,b_string,c_string,d_string,DRAW_NORMAL,DRAW_NORMAL,DRAW_NORMAL,DRAW_INVERT);
	break;
	}
}

/**
 *
 *
 * 数值显示反转函数
 */
void valueDispInvert(float dispValue,unsigned int invertAt){
	if(dispValue < 10){												//反转显示频率值某一位
				DispSmall916WithOneInvert(3,35,dispValue,1,6,invertAt);
			}else if(freq_disp < 100){
				DispSmall916WithOneInvert(3,35,dispValue,2,6,invertAt);
			}else
				DispSmall916WithOneInvert(3,35,dispValue,3,5,invertAt);

}

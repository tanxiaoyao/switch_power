module SP_TOP(
	input clk_50M,RST_N,
	input [3:2]CLK,
	input WR,RD,
	input [11:0]ADDR,
	inout [15:0]DATA,
	output [3:0]INT,
	inout [23:0]GPIO_A,
	inout [23:0]GPIO_B
);

wire [15:0]rddat0;
wire software_rst_n;
wire cs0,cs1,cs2,cs3;//cs4,cs5,cs6,cs7;
wire [15:0]wrdat;
wire [7:0]addr;
wire [23:0]addr24;

pll	pll_inst (
	.inclk0 ( clk_50M ),
	.c0 ( clk_100M )
	);

SP_BUS SP_BUS_inst
(
	.clk(clk_100M) ,	// input  clk_sig
	.rst_n(RST_N) ,	// input  rst_n_sig
	.ADDR(ADDR) ,	// input [11:0] ADDR_sig
	.RD(RD) ,	// input  RD_sig
	.WR(WR) ,	// input  WR_sig
	.DATA(DATA) ,	// inout [15:0] DATA_sig
	.software_rst_n(software_rst_n),
	.cs0(cs0) ,	// output  cs0_sig
	.cs1(cs1) ,	// output  cs1_sig
	.cs2(cs2) ,	// output  cs2_sig
	.cs3(cs3) ,	// output  cs3_sig
	.addr(addr) ,	// output [7:0] addr_sig
	.addr24(addr24) ,	// output [23:0] addr24_sig
	.rddat0(rddat0) ,	// input [15:0] rddat0_sig
	.rddat1(rddat1) ,	// input [15:0] rddat1_sig
	.rddat2(rddat2) ,	// input [15:0] rddat2_sig
	.rddat3(rddat3) ,	// input [15:0] rddat3_sig
	.wrdat(wrdat) 	// output [15:0] wrdat_sig
);

SP_DUTY_TEST SP_DUTY_TEST_inst
(
	.clk(clk_100M) ,	// input  clk_sig
	.rst_n(RST_N) ,	// input  rst_n_sig
	.catin(GPIO_B[0]) ,	// input  catin_sig
	.cs(cs0) ,	// input  cs_sig
	.wr(WR) ,	// input  wr_sig
	.rd(RD) ,	// input  rd_sig
	.addr(addr[2:0]) ,	// input [2:0] addr_sig
	.rddat(rddat0) 	// output [15:0] rddat_sig
);

endmodule
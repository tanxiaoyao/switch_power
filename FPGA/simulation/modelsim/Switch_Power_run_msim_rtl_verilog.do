transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+F:/GitSync/switch_power/FPGA {F:/GitSync/switch_power/FPGA/SP_DUTY_TEST.v}

vlog -vlog01compat -work work +incdir+F:/GitSync/switch_power/FPGA/simulation/modelsim {F:/GitSync/switch_power/FPGA/simulation/modelsim/SP_DUTY_TEST.vt}

vsim -t 1ps -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L cycloneii_ver -L rtl_work -L work -voptargs="+acc"  SP_DUTY_TEST_vlg_tst

add wave *
view structure
view signals
run 1 ms

library verilog;
use verilog.vl_types.all;
entity SP_DUTY_TEST is
    port(
        clk             : in     vl_logic;
        rst_n           : in     vl_logic;
        catin           : in     vl_logic;
        cs              : in     vl_logic;
        wr              : in     vl_logic;
        rd              : in     vl_logic;
        addr            : in     vl_logic_vector(2 downto 0);
        rddat           : out    vl_logic_vector(15 downto 0)
    );
end SP_DUTY_TEST;
